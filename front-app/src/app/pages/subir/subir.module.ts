import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubirRoutingModule } from './subir-routing.module';
import { SubirComponent } from './subir.component';


@NgModule({
  declarations: [SubirComponent],
  imports: [
    CommonModule,
    SubirRoutingModule
  ]
})
export class SubirModule { }
