import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubirComponent } from './subir.component';

const routes: Routes = [{ path: '', component: SubirComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubirRoutingModule { }
