import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';

import { PixabayModifiedResponse } from '../../models/pixabay-apiresponse';

import { RequestImgService } from '../../services/request-img.service';

@Component({
  selector: 'app-inspiracion',
  templateUrl: './inspiracion.component.html',
  styleUrls: ['./inspiracion.component.scss']
})
export class InspiracionComponent implements OnInit {
  public imagesList: PixabayModifiedResponse[] = [];

  constructor(private  requestImgService: RequestImgService) { }

  ngOnInit(): void {
    this.requestImgService.getImages().subscribe(
      (result: PixabayModifiedResponse) => {
        this.imagesList.push(result);
        console.log(result);
      },
      (err) => {
        console.error(err.message);
      }
    );

  }

}
