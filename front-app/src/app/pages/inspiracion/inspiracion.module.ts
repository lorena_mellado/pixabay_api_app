import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InspiracionRoutingModule } from './inspiracion-routing.module';
import { InspiracionComponent } from './inspiracion.component';


@NgModule({
  declarations: [InspiracionComponent],
  imports: [
    CommonModule,
    InspiracionRoutingModule
  ]
})
export class InspiracionModule { }
