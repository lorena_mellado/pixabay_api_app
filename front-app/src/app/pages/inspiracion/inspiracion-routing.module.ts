import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InspiracionComponent } from './inspiracion.component';

const routes: Routes = [{ path: '', component: InspiracionComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InspiracionRoutingModule { }
