import { Component, OnInit } from '@angular/core';

import { RequestImgService } from '../../services/request-img.service';
import { PixabayModifiedResponse } from '../../models/pixabay-apiresponse';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() { 
 
  };

  ngOnInit(): any {

  }
}
