import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [

  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: () =>
      import('./pages/home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'subir',
    loadChildren: () =>
      import('./pages/subir/subir.module').then((m) => m.SubirModule),
  },
  {
    path: 'inspiracion',
    loadChildren: () =>
      import('./pages/inspiracion/inspiracion.module').then((m) => m.InspiracionModule),
  },
  {
    path: 'buscar',
    loadChildren: () =>
      import('./pages/buscar/buscar.module').then((m) => m.BuscarModule),
  },
  {
    path: 'categoria',
    loadChildren: () =>
      import('./pages/categoria/categoria.module').then((m) => m.CategoriaModule),
  },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
