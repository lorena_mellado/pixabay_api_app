import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { PixabayAPIResponse, PixabayModifiedResponse } from '../models/pixabay-apiresponse';


@Injectable({
  providedIn: 'root'
})
export class RequestImgService {

  

  private urlAPI = "https://pixabay.com/api/?key="+this.keyAPI; 

  public imgList: PixabayModifiedResponse[] = [];

  constructor(private http: HttpClient) {}

  getImages(){
    return this.http.get(this.urlAPI).pipe(
      map((response: any) => { 
        if (!response){
          throw new Error('Value expected!'); 
        }else{
          const formattedResults: PixabayModifiedResponse = {
            id: response.id,
            tags: response.tags,
            largeImageURL: response.largeImageURL,
          }
        return formattedResults;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    )
  } 

  /*getAPIResponse(){
    this.http.get(this.urlAPI).subscribe(data => {
    console.log(data);
  });
  console.log("Esto se ejecutará antes que el console log de arriba");*/
}
  
  