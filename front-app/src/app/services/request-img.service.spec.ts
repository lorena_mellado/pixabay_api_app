import { TestBed } from '@angular/core/testing';

import { RequestImgService } from './request-img.service';

describe('RequestImgService', () => {
  let service: RequestImgService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RequestImgService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
